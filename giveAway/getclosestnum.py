import random
import pandas as pd
import requests
import io


def closest_number(input_list, choose_number):
    closest = None
    difference = 9999999999
    previous_difference = difference
    for input_number in input_list:
        difference = abs(input_number - choose_number)
        if difference < previous_difference:
            previous_difference = difference
            closest = input_number


    return closest

def rollUntilGet(lst):
    count = 0
    numberRange = list(range(1, 1001))
    while True:
        if numberRange:
            randomNumber = random.choice(numberRange)
            if randomNumber in lst:
                return randomNumber, count
            else:
                numberRange.remove(randomNumber)
        else:
            print("\n\nList bé quá đéo chọn được.")
            return "None", count
        count += 1

def giveReward(inputDict, prizes):
    while prizes:
        for name in inputDict:
            if prizes:
                reward = random.choice(prizes)
                inputDict[name] += reward + " "
                prizes.remove(reward)
            else:
                break
    return inputDict


def rollWithInfoFromXlsx(url, name_col, num_col):
    r = requests.get(url)

    df = pd.read_excel(io.BytesIO(r.content), engine='openpyxl',usecols=[name_col, num_col])
    lstNum = df.loc[~df[num_col].isna(), num_col].values

    giveNumber, tried = rollUntilGet(lstNum)
    print(f'\nWinning Number: {giveNumber}\nTried: {tried}')

    pplGet = df.loc[(df[num_col] == giveNumber, name_col)].values
    print(pplGet)






def main():
    url = "https://docs.google.com/spreadsheets/d/11ISFVReM1nXAM7lrquxg9DE8oifEDmIu/export?format=xlsx"
    rollWithInfoFromXlsx(url, name_col="PPL", num_col="NUM")

if __name__ == "__main__":
    main()
